/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imp;

import com.models.Persona;
import com.utils.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author fredy.alfarousam
 */
public class PersonaFacade extends AbstractFacade<Persona>{

    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonaFacade() {
        super(Persona.class);
        em=Persistence.createEntityManagerFactory("conn").createEntityManager();
    }
    
}
