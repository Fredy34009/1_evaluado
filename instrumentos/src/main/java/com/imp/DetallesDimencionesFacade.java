/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imp;

import com.models.DetallesDimenciones;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author fredy.alfarousam
 */
public class DetallesDimencionesFacade extends AbstractFacade<DetallesDimenciones> {

   
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetallesDimencionesFacade() {
        super(DetallesDimenciones.class);
        em=Persistence.createEntityManagerFactory("conn").createEntityManager();
    }
    
}
