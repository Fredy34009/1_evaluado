/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imp;

import com.models.Dimensiones;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author fredy.alfarousam
 */
public class DimensionesFacade extends AbstractFacade<Dimensiones> {

    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DimensionesFacade() {
        super(Dimensiones.class);
        em=Persistence.createEntityManagerFactory("conn").createEntityManager();
    }
    
}
