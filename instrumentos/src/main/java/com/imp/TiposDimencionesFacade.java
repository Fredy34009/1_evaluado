/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imp;

import com.models.TiposDimenciones;
import com.utils.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author fredy.alfarousam
 */
public class TiposDimencionesFacade extends AbstractFacade<TiposDimenciones>{

    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TiposDimencionesFacade() {
        super(TiposDimenciones.class);
        em=Persistence.createEntityManagerFactory("conn").createEntityManager();
    }
}
