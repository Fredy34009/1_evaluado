/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imp;

import com.models.NivelPersonal;
import com.utils.Dao;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author fredy.alfarousam
 */
public class NivelPersonalFacade extends AbstractFacade<NivelPersonal> implements Dao,Serializable{

   
    private EntityManager em;

    public NivelPersonalFacade(Class<NivelPersonal> entityClass) {
        super(entityClass);
        em=Persistence.createEntityManagerFactory("conn").createEntityManager();
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
