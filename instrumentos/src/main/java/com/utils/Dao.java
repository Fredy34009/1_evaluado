/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils;

import com.models.NivelPersonal;
import java.util.List;

/**
 *
 * @author fredy.alfarousam
 * @param <T>
 */
public interface Dao {
    
    public void crear(NivelPersonal entity);
    public void editar(NivelPersonal entity);
    public void eliminar(NivelPersonal entity);
    public List<NivelPersonal> findAll();
}
