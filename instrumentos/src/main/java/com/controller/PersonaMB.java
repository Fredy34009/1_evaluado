/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.imp.NivelPersonalFacade;
import com.imp.PersonaFacade;
import com.models.NivelPersonal;
import com.models.Persona;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author fredy.alfarousam
 */
@ManagedBean(name = "personaMB")
@ViewScoped
public class PersonaMB implements Serializable{
    
    private NivelPersonalFacade  dao;
    private List<NivelPersonal> nivelPersonals;
    private NivelPersonal nivelPersonal;
    private Persona p;
    
    //Getter and setter

    public List<NivelPersonal> getNivelPersonals() {
        nivelPersonals=new NivelPersonalFacade(NivelPersonal.class).findAll();
        return nivelPersonals;
    }

    public void setNivelPersonals(List<NivelPersonal> nivelPersonals) {
        this.nivelPersonals = nivelPersonals;
    }

    public NivelPersonal getNivelPersonal() {
        
        return nivelPersonal;
    }

    public void setNivelPersonal(NivelPersonal nivelPersonal) {
        this.nivelPersonal = nivelPersonal;
    }

    public Persona getP() {
        return p;
    }

    public void setP(Persona p) {
        this.p = p;
    }
    
    @PostConstruct
    public void init()
    {
        nivelPersonal=new NivelPersonal();
        p=new Persona();
        dao=new NivelPersonalFacade(NivelPersonal.class);
    }
    //Crea un nivel Personal
    public void crear()
    {
        try {
            
            dao.crear(nivelPersonal);
            System.out.println("Exito nivel creado");
        } catch (Exception e) {
            System.out.println("Error nivel no creado"+e.getMessage());
        }
    }
    //Lista los niveles de Personal
    public void listar()
    {
        try {
            nivelPersonals=dao.findAll();
        } catch (Exception e) {
        }
    }
    public void eliminar(NivelPersonal np)
    {
        
        try {
            this.nivelPersonal=np;
            dao.eliminar(np);
            System.out.println("Exito eliminado");
        } catch (Exception e) {
        }
    }
    //crea una persona
    public void crearPersona()
    {
        try {
            PersonaFacade personaFacade=new PersonaFacade();
            p.setNivelPersonal(nivelPersonal);
            personaFacade.crear(p);
        } catch (Exception e) {
        }
        
    }
}
