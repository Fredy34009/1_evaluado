/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author fredy.alfarousam
 */
@Entity
@Table(name = "detalles_dimenciones")
public class DetallesDimenciones implements Serializable {

   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    
    @JoinColumn(name = "dimension")
    @ManyToOne
    private Dimensiones dimension;
    
    @JoinColumn(name = "criterio")
    @ManyToOne
    private Criterios criterio;
    
    @JoinColumn(name = "persona")
    @ManyToOne(optional = false)
    private Persona persona;

    public DetallesDimenciones() {
    }

    public DetallesDimenciones(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Dimensiones getDimension() {
        return dimension;
    }

    public void setDimension(Dimensiones dimension) {
        this.dimension = dimension;
    }

    public Criterios getCriterio() {
        return criterio;
    }

    public void setCriterio(Criterios criterio) {
        this.criterio = criterio;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetallesDimenciones)) {
            return false;
        }
        DetallesDimenciones other = (DetallesDimenciones) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.models.DetallesDimenciones[ id=" + id + " ]";
    }
    
}
