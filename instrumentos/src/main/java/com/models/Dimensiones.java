/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fredy.alfarousam
 */
@Entity
@Table(name = "dimensiones")
public class Dimensiones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "pregunta")
    private String pregunta;
    @JoinColumn(name = "tipo_dimencion")
    @ManyToOne(optional = false)
    private TiposDimenciones tipoDimencion;
    @OneToMany(mappedBy = "dimension")
    private List<DetallesDimenciones> detallesDimencionesList;

    public Dimensiones() {
    }

    public Dimensiones(Integer id) {
        this.id = id;
    }

    public Dimensiones(Integer id, String pregunta) {
        this.id = id;
        this.pregunta = pregunta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public TiposDimenciones getTipoDimencion() {
        return tipoDimencion;
    }

    public void setTipoDimencion(TiposDimenciones tipoDimencion) {
        this.tipoDimencion = tipoDimencion;
    }

    public List<DetallesDimenciones> getDetallesDimencionesList() {
        return detallesDimencionesList;
    }

    public void setDetallesDimencionesList(List<DetallesDimenciones> detallesDimencionesList) {
        this.detallesDimencionesList = detallesDimencionesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dimensiones)) {
            return false;
        }
        Dimensiones other = (Dimensiones) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.models.Dimensiones[ id=" + id + " ]";
    }
    
}
