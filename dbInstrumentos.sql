create database instrumentos;
use instrumentos;

create table nivel_personal(
id int primary key auto_increment not null,
nivel varchar(50) not null
);

create table persona(
id int primary key auto_increment not null,
nombre varchar(50) not null,
edad int not null,
sexo enum('f','m') not null,
escolaridad int not null,
nivel_personal int not null,
foreign key(nivel_personal) references nivel_personal(id)
);

create table tipos_dimenciones(
id int primary key auto_increment not null,
tipo varchar(50) not null
);

create table dimensiones(
id int primary key auto_increment not null,
pregunta varchar(150) not null,
tipo_dimencion int not null,

foreign key(tipo_dimencion) references tipos_dimenciones(id)
); 

create table criterios(
id int primary key auto_increment not null,
criterio varchar(25)
);

create table detalles_dimenciones(
id int primary key auto_increment not null,
dimension int,
criterio int,
persona int not null,

foreign key(dimension) references dimensiones(id),
foreign key(criterio) references criterios(id),
foreign key(persona) references persona(id)
)