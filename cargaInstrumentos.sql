INSERT INTO `instrumentos`.`nivel_personal` (`nivel`) VALUES ('Operario');
INSERT INTO `instrumentos`.`nivel_personal` (`nivel`) VALUES ('Administrador');
INSERT INTO `instrumentos`.`nivel_personal` (`nivel`) VALUES ('Jefe');
INSERT INTO `instrumentos`.`nivel_personal` (`nivel`) VALUES ('Gerente');

INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Nada');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Poco');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Termino Medio');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Bastante');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Mucho');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Malas');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Regulares');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Adecuadas');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Esta bien');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Excelente');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Mala');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Regular');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Buena');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Muy Buena');
INSERT INTO `instrumentos`.`criterios` (`criterio`) VALUES ('Excelente');

INSERT INTO `instrumentos`.`tipos_dimenciones` (`tipo`) VALUES ('Laboral');
INSERT INTO `instrumentos`.`tipos_dimenciones` (`tipo`) VALUES ('Pertenecia y Comp.');

INSERT INTO `instrumentos`.`dimensiones` (`pregunta`, `tipo_dimencion`) VALUES ('Su espacio le resulta agradable', '1');
INSERT INTO `instrumentos`.`dimensiones` (`pregunta`, `tipo_dimencion`) VALUES ('Como califica sus horarios de trabajo', '1');
INSERT INTO `instrumentos`.`dimensiones` (`pregunta`, `tipo_dimencion`) VALUES ('Le gusta trabajar aqui', '2');
INSERT INTO `instrumentos`.`dimensiones` (`pregunta`, `tipo_dimencion`) VALUES ('Como califica el lugar de trabajo', '2');

INSERT INTO `instrumentos`.`persona` (`nombre`, `edad`, `sexo`, `escolaridad`, `nivel_personal`) VALUES ('Fredy Alfaro', '23', 'm', '2', '1');
INSERT INTO `instrumentos`.`persona` (`nombre`, `edad`, `sexo`, `escolaridad`, `nivel_personal`) VALUES ('Juan Portillo', '21', 'm', '1', '2');
INSERT INTO `instrumentos`.`persona` (`nombre`, `edad`, `sexo`, `escolaridad`, `nivel_personal`) VALUES ('Isabel Santos', '35', 'f', '1', '3');
INSERT INTO `instrumentos`.`persona` (`nombre`, `edad`, `sexo`, `escolaridad`, `nivel_personal`) VALUES ('Isabel Hernandez', '19', 'f', '1', '4');

INSERT INTO `instrumentos`.`detalles_dimenciones` (`dimension`, `criterio`, `persona`) VALUES ('1', '2', '1');
INSERT INTO `instrumentos`.`detalles_dimenciones` (`dimension`, `criterio`, `persona`) VALUES ('1', '1', '2');
INSERT INTO `instrumentos`.`detalles_dimenciones` (`dimension`, `criterio`, `persona`) VALUES ('1', '2', '3');
INSERT INTO `instrumentos`.`detalles_dimenciones` (`dimension`, `criterio`, `persona`) VALUES ('3', '1', '4');

